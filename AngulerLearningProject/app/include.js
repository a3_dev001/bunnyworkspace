module.exports = function(app){
	
	require('./modules/controller_test.js')(app);

	require('./modules/service_test.js')(app);
	
	require('./modules/factory_test.js')(app);
	
	require('./modules/service_class_test.js')(app);
	
};