require('angular');

var app =  angular.module('app',[]);

require('./include.js')(app);

app.controller('MainController',  function($scope){
	$scope.message = 'Chal Gya re !!! ';
});


app.controller('bunnyServiceConntroller',function(bunnyService){
	bunnyService.add(3,2); // Show logs
});


app.controller('bunnyServiceClassConntroller',function(bunnyClassService){
	bunnyClassService.add(3,3); // Show logs
});


app.controller('bunnyFactoryConntroller',function(myFactory,$scope){
	$scope.myFactoryMessage = myFactory.sayHello('BuNnY');
});
