module.exports = function(app){
	app.service('bunnyService',  function(){
		// service is just a constructor function
		// that will be called with 'new'

		this.add = function(a,b){
			console.log('My initial logger for bunnyService with add function !!!!');
			return (a+b);
		};
	});
}