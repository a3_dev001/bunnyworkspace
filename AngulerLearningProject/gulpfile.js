var gulp = require('gulp');
var sass = require('gulp-ruby-sass');
var connect = require('gulp-connect');

// requires browserify and vinyl-source-stream
var browserify = require('browserify');
var source = require('vinyl-source-stream')

gulp.task('connect',function(){
	connect.server({
		root : 'public',
		port : 1439
	});
});

gulp.task('browserify',function(){
	// Grab the app.js file 
	return browserify('./app/app.js')
	// bundle it and create a main.js file in public folder
	.bundle()
	.pipe(source('main.js'))
	// save it the public/js directories
	.pipe(gulp.dest('./public/js/'));
});

gulp.task('watch',function(){
	gulp.watch('app/**/*.js',['browserify']);
});

//gulp.task('default', ['connect','watch']);
gulp.task('bunny', ['connect','watch']);